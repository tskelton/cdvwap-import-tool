﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace CDVWAPImportTool
{
    public partial class CDVWAPImportTool : Form
    {
        public CDVWAPImportTool()
        {
            InitializeComponent();
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void SqlBatch_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();

            diag.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            diag.FilterIndex = 1;

            if(diag.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = diag.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            connection.Open();

            FileInfo file = new FileInfo(textBox1.Text);

            button1.Enabled = false;
            button2.Enabled = false;

            using(var reader = new StreamReader(file.OpenRead()))
            {
                var lineCount = File.ReadLines(file.FullName).Count();
                progressBar1.Minimum = 0;
                progressBar1.Maximum = lineCount;
                string line = string.Empty;
                int count = 0;
                label4.Text = file.Name;

                while ((line = reader.ReadLine()) != null)
                {
                    StringBuilder script = new StringBuilder();
                    //AA,2015-05-18,13.385956651635542,13965302
                    var values = line.Split(',');

                    script.AppendFormat("INSERT INTO {0} ([{1}],[{2}],[{3}],[{4}],[{5}],[{6}],[{7}],[{8}],[{9}],[{10}],[{11}],[{12}])",
                        ConfigurationManager.AppSettings["Table"],
                        "AddDate",
                        "TransactionDate",
                        "CurrentDayVolumeWeightedAveragePrice",
                        "CurrentDayVolumeSum",
                        "DividendAdjustment",
                        "TrendRatioTradesOpen",
                        "TrendRatioHigh",
                        "TrendRatioLow",
                        "TrendRatioTradesClosed",
                        "Symbol",
                        "MaximumDayRatio",
                        "MinimumDayRatio"
                        );

                    script.AppendFormat(" VALUES ('{0}','{1}',{2},{3},{4},{5},{6},{7},{8},'{9}',{10},{11});",
                        /*Add Date*/ DateTime.Now.ToShortDateString(),
                        /*TransactionDate*/ values[1],
                        /*CurrentDayVolumeWeightedAveragePrice*/ values[2],
                        /*CurrentDayVolumeSum*/ values[3],
                        /*DividendAdjustment*/ 0,
                        /*TrendRatioTradesOpen*/ 0,
                        /*TrendRatioHigh*/ 0,
                        /*TrendRatioLow*/ 0,
                        /*TrendRatioTradesClosed*/ 0,
                        /*Symbol*/ values[0],
                        /*MaximumDayRatio*/ 0,
                        /*MinimumDayRatio*/ 0
                        );

                    try
                    {
                        SqlCommand command = new SqlCommand(script.ToString(), connection);
                        command.ExecuteNonQuery();
                        label5.Text = count.ToString();
                        script.Clear();
                    }
                    catch (SqlException sqle)
                    {
                        DialogResult result = MessageBox.Show(sqle.Message + "\r\nWould you like to continue?", "Sql Exception", MessageBoxButtons.YesNo);
                        if (result == DialogResult.No)
                        {
                            break;
                        }
                        script.Clear();
                    }

                    ++count;
                    progressBar1.Value = count;
                    Application.DoEvents();
                }
            }

            connection.Close();
            MessageBox.Show("Process Complete!", "Result", MessageBoxButtons.OK);

            button1.Enabled = true;
            button2.Enabled = true;
        }
    }
}
